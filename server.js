//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//
require('dotenv').config()

const http = require('http');
const path = require('path');

const async = require('async');
const socketio = require('socket.io');
const express = require('express');

const app = express();
const server = http.createServer(app);
const io = socketio.listen(server);

const messages = [];
const sockets = [];

app.use(express.static(path.resolve(__dirname, 'client')));

io.on('connection', function (socket) {
  messages.forEach(function (data) {
    socket.emit('message', data);
  });

  sockets.push(socket);

  socket.on('disconnect', function () {
    sockets.splice(sockets.indexOf(socket), 1);
    updateRoster();
  });

  socket.on('message', function (msg) {
    const text = String(msg || '');

    if (!text)
      return;

    socket.get('name', function (err, name) {
      const data = {
        name: name,
        text: text
      };

      broadcast('message', data);
      messages.push(data);
    });
  });

  socket.on('identify', function (name) {
    socket.set('name', String(name || 'Anonimo'), function (err) {
      updateRoster();
    });
  });
});

function updateRoster() {
  async.map(
    sockets,
    function (socket, callback) {
      socket.get('name', callback);
    },
    function (err, names) {
      broadcast('roster', names);
    }
  );
}

function broadcast(event, data) {
  sockets.forEach(function (socket) {
    socket.emit(event, data);
  });
}

server.listen(process.env.PORT || 3000, process.env.IP || "127.0.0.1", function(){
  const addr = server.address();
  console.log("Chat server listening at", addr.address + ":" + addr.port);
});
