# [Chatnodejs](https://repl.it/@SorielV/MinimalistChat)

```
██████╗ ███████╗██████╗ ██╗        ██╗████████╗
██╔══██╗██╔════╝██╔══██╗██║        ██║╚══██╔══╝
██████╔╝█████╗  ██████╔╝██║        ██║   ██║   
██╔══██╗██╔══╝  ██╔═══╝ ██║        ██║   ██║   
██║  ██║███████╗██║     ███████╗██╗██║   ██║   
╚═╝  ╚═╝╚══════╝╚═╝     ╚══════╝╚═╝╚═╝   ╚═╝   
```

## env

```bash
# .env
PORT=8080
IP=127.0.0.1
```

```bash
npm i
npm start
```

## #1
7e9ba1d35a5fb31110076caa3cedd21714f3d4b4

## #2 y #3
0014793ab5c10a386039848666282ca867551f98

![#2 y @3](https://i.imgur.com/mNNtbQx.png)




